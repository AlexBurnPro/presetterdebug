package com.disgroup.pim.customizing.presetterdebug.internal;

import java.nio.charset.StandardCharsets;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.util.EDataTypeEList;

import com.heiler.ppm.acl.commons.AclFlags;
import com.heiler.ppm.acl.core.entity.AclProxy;
import com.heiler.ppm.repository.Entity;
import com.heiler.ppm.repository.EntityType;
import com.heiler.ppm.repository.Field;
import com.heiler.ppm.repository.FieldType;
import com.heiler.ppm.repository.core.RepositoryComponent;
import com.heiler.ppm.repository.core.RepositoryService;
import com.heiler.ppm.repository.path.EntityPath;
import com.heiler.ppm.repository.path.FieldPath;
import com.heiler.ppm.security.core.LoginToken;
import com.heiler.ppm.std.core.command.CommandContext;
import com.heiler.ppm.std.core.entity.EntityDetailModel;
import com.heiler.ppm.std.core.entity.filter.LoadHint;
import com.heiler.ppm.std.core.entity.filter.LoadHintBuilder;
import com.heiler.ppm.std.core.internal.command.operator.EntityPermissionChecker;
import com.heiler.ppm.std.core.util.EObjectUtils;
import com.heiler.ppm.usermanagement.core.entity.UserGroupProxy;
import com.heiler.ppm.usermanagement.core.entity.UserProxy;

public class InitiatorPlantPermissionChecker  implements EntityPermissionChecker {
	private static final Log log = LogFactory.getLog(InitiatorPlantPermissionChecker.class);
	private static final RepositoryService REPOSITORY_SERVICE = RepositoryComponent.getRepositoryService();
	@Override
	public void validateEntity(CommandContext commandContext, LoginToken loginToken, Qualification[] qualification)
			throws CoreException {
		log.debug("Setting initiator plant or be");
		
		if (loginToken == null) {
			log.error("loginToken = null!!!");
			return;
		}
		UserProxy userProxy = new UserProxy(loginToken.getInternalId());

		EntityType userEntityType = userProxy.getEntityType();
		EntityPath userEntityPath = new EntityPath(userEntityType);
		LoadHint userLoadHint = new LoadHintBuilder(userEntityType).add(userEntityPath).build();
		
		Field userGroupsField = userProxy.getEntity().getField("User.UserGroups", true);
		FieldType userGroupsFieldType = userGroupsField.getFieldType();
		FieldPath userGroupsFieldPath = new FieldPath(userGroupsFieldType);
		
		FieldPath userFirstNameFieldPath = new FieldPath(userProxy.getEntity().getField("User.FirstName", true).getFieldType());
		FieldPath userLastNameFieldPath = new FieldPath(userProxy.getEntity().getField("User.LastName", true).getFieldType());
		FieldPath userEmailFieldPath = new FieldPath(userProxy.getEntity().getField("User.Email", true).getFieldType());
		
		FieldPath aclProxyFieldPath = new FieldPath(REPOSITORY_SERVICE.getFieldTypeByIdentifier("ArticleType.AclProxy"));
		FieldPath aclFlagFieldPath = new FieldPath(REPOSITORY_SERVICE.getFieldTypeByIdentifier("ArticleType.AclFlag"));
		LoadHint articleLoadHint = new LoadHintBuilder(commandContext.getEntityItem().getEntityType()).build(); 
		EntityDetailModel articleDetailModel = commandContext.getEntityItem().getEntityProxy().getDetailModel(LoadHint.EVERYTHING);
		AclProxy aclProxy = (AclProxy) articleDetailModel.getFieldValue(aclProxyFieldPath);
		AclFlags aclFlags = (AclFlags) articleDetailModel.getFieldValue(aclFlagFieldPath);
		if (aclProxy != null) {
			log.debug("Article aclProxy info-tree:");
			log.debug(EObjectUtils.printTree(aclProxy.getEntityType(), aclProxy.getDetailModel(LoadHint.EVERYTHING).getDataObject()));
		}
		if (aclFlags != null) {
			log.debug("Article aclFlag info:" + aclFlags.toString());
		}
		
		
		try {
			EntityDetailModel userEntityDetailModel = userProxy.getDetailModel(userLoadHint);
			try {
				userEntityDetailModel.acquireRead();
				EDataTypeEList<UserGroupProxy> userGroups = (EDataTypeEList<UserGroupProxy>) userEntityDetailModel.getFieldValue(userGroupsFieldPath);
				String userFirstName = (String) userEntityDetailModel.getFieldValue(userFirstNameFieldPath);
				String userLastName = (String) userEntityDetailModel.getFieldValue(userLastNameFieldPath);
				String userEmail = (String) userEntityDetailModel.getFieldValue(userEmailFieldPath);
				
				
				String debug = "Printing rights of the user: " + userFirstName + " " + userLastName + ", email=" + userEmail;
				byte[] bytesDebug = debug.getBytes(StandardCharsets.UTF_8);

				String utf8EncodedDebug = new String(bytesDebug, StandardCharsets.UTF_8);
				log.debug(utf8EncodedDebug);
				
				for (UserGroupProxy userGroupProxy : userGroups) {
					Entity userGroupProxyEntity = userGroupProxy.getEntity();
					FieldPath userGroupNameFieldPath = new FieldPath(
							userGroupProxyEntity.getField("UserGroup.Name", true).getFieldType());
					FieldPath userGroupIdentifierFieldPath = new FieldPath(
							userGroupProxyEntity.getField("UserGroup.Identifier", true).getFieldType());
//					FieldPath userGroupPermissionsFieldPath = new FieldPath(
//							userGroupProxyEntity.getField("UserGroup.Permissions", true).getFieldType());
//					FieldPath userGroupPermissions2FieldPath = new FieldPath(
//							userGroupProxyEntity.getField("UserGroup.Permissions2", true).getFieldType());
					
					String userGroupName = (String) userGroupProxy.getDetailModel(LoadHint.EVERYTHING).getFieldValue(userGroupNameFieldPath);
					String userGroupIdentifier = (String) userGroupProxy.getDetailModel(LoadHint.EVERYTHING).getFieldValue(userGroupIdentifierFieldPath);
//					String userGroupPermissions = (String) userGroupProxy.getDetailModel(LoadHint.EVERYTHING).getFieldValue(userGroupPermissionsFieldPath);
//					String userGroupPermissions2 = (String) userGroupProxy.getDetailModel(LoadHint.EVERYTHING).getFieldValue(userGroupPermissions2FieldPath);
					String debug2 = "    userGroupName=" + userGroupName + ", userGroupIdentifier=" + userGroupIdentifier;
					byte[] bytesDebug2 = debug2.getBytes(StandardCharsets.UTF_8);

					String utf8EncodedDebug2 = new String(bytesDebug2, StandardCharsets.UTF_8);
					log.debug(utf8EncodedDebug2);

				}
				
			} finally {
				userEntityDetailModel.releaseRead();
			}
		} catch (CoreException e) {
			log.error("An error occurred while checking users rights.", e);
			e.printStackTrace();
		}
	
	}

}
